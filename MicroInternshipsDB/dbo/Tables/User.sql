﻿CREATE TABLE [dbo].[User]
(
	[UserID] INT NOT NULL PRIMARY KEY,
	[NNum] INT NOT NULL,
    [FirstName] VARCHAR(50) NOT NULL, 
    [LastName] VARCHAR(50) NOT NULL, 
    [Email] VARCHAR(MAX) NOT NULL, 
)
