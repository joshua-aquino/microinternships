﻿CREATE TABLE [dbo].[Project]
(
	[ProjectID] INT NOT NULL PRIMARY KEY IDENTITY,
    [Title] VARCHAR(50) NOT NULL, 
    [Description] VARCHAR(MAX) NOT NULL, 
    [Topic] VARCHAR(50) NULL
)
