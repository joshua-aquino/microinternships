﻿using DataLibrary.BusinessLogic;
using MicroInternshipsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MicroInternshipsApp.Controllers
{
    public class AdministratorController : Controller
    {
        // GET: Administrator
        public ActionResult Index()
        {
            ViewBag.Message = "Administrator";

            return View();
        }
        public ActionResult ViewUsers()
        {
            ViewBag.Message = "User List";

            var data = UserProcessor.LoadUsers();
            List<UserModel> users = new List<UserModel>();

            foreach (var row in data)
            {
                users.Add(new UserModel
                {
                    NNum = row.NNum,
                    FirstName = row.FirstName,
                    LastName = row.LastName,
                    Email = row.Email
                });
            }

            return View(users);
        }
        public ActionResult ViewProjects()
        {
            ViewBag.Message = "Project List";

            var data = ProjectProcessor.LoadProjects();
            List<ProjectModel> projects = new List<ProjectModel>();

            foreach (var row in data)
            {
                projects.Add(new ProjectModel 
                {
                    Title = row.Title,
                    Description = row.Description,
                    Topic = row.Topic
                });
            }

            return View(projects);
        }
        public ActionResult ViewOrganization()
        {
            ViewBag.Message = "Organization List";

            var data = OrganizationProcessor.LoadOrganization();
            List<OrganizationModel> organizations = new List<OrganizationModel>();

            foreach (var row in data)
            {
                organizations.Add(new OrganizationModel 
                {
                    OrganizationName = row.OrganizationName,
                    OrganizationType = row.OrganizationType,
                    OrganizationWebsite = row.OrganizationWebsite
                });
            }

            return View(organizations);
        }
    }
}