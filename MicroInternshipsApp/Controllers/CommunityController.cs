﻿using MicroInternshipsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLibrary;
using DataLibrary.BusinessLogic;

namespace MicroInternshipsApp.Controllers
{
    public class CommunityController : Controller
    {
        // GET: Community
        public ActionResult Index()
        {
            ViewBag.Message = "Community User";

            return View();
        }
        public ActionResult CreateProject()
        {
            ViewBag.Message = "Create Project";

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProject(ProjectModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = ProjectProcessor.CreateProject(model.Title, model.Description, model.Topic);
                return RedirectToAction("Index");
            }

            return View();
        }
        public ActionResult NewOrganization()
        {
            ViewBag.Message = "Register Organization";

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewOrganization(OrganizationModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = OrganizationProcessor.NewOrganization(model.OrganizationName,
                   model.OrganizationType, model.OrganizationWebsite);
                return RedirectToAction("Index");
            }

            return View();
        }
    }
}