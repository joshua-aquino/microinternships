﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MicroInternshipsApp.Models
{
    public class OrganizationModel
    {
        [Display(Name = "Organization Name")]
        [Required(ErrorMessage = "Please enter a name")]
        public string OrganizationName { get; set; }
        [Display(Name = "Organization Type")]
        [Required(ErrorMessage = "Is the organization for profit or non-profit")]
        public string OrganizationType { get; set; }
        [Display(Name = "Website")]
        public string OrganizationWebsite { get; set; }
    }
}