﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MicroInternshipsApp.Models
{
    public class ProjectModel
    {
        [Display(Name = "Title")]
        [Required(ErrorMessage = "Please enter a title")]
        public string Title { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please enter a description")]
        public string Description { get; set; }
        [Display(Name = "Topic")]
        public string Topic { get; set; }
    }
}