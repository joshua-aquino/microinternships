﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class OrganizationModel
    {
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationType { get; set; }
        public string OrganizationWebsite { get; set; }
    }
}
