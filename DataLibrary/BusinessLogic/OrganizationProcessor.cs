﻿using DataLibrary.DataAccess;
using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLibrary.BusinessLogic
{
    public static class OrganizationProcessor
    {
        public static int NewOrganization(string organizationName, string organizationType, 
            string organizationWebsite)
        {
            OrganizationModel data = new OrganizationModel
            {
                OrganizationName = organizationName,
                OrganizationType = organizationType,
                OrganizationWebsite = organizationWebsite
            };

            string sql = @"insert into [dbo].[Organization] (OrganizationName, OrganizationType, OrganizationWebsite)
                           values (@OrganizationName, @OrganizationType, @OrganizationWebsite);";

            return SQLDataAccess.SaveData(sql, data);
        }

        public static List<OrganizationModel> LoadOrganization()
        {
            string sql = @"select OrganizationName, OrganizationType, OrganizationWebsite 
                            from [dbo].[Organization];";

            return SQLDataAccess.LoadData<OrganizationModel>(sql);
        }
    }
}
