﻿using DataLibrary.DataAccess;
using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLibrary.BusinessLogic
{
    public static class ProjectProcessor
    {
        public static int CreateProject(string title, string description, 
            string topic)
        {
            ProjectModel data = new ProjectModel
            {
                Title = title,
                Description = description,
                Topic = topic
            };

            string sql = @"insert into [dbo].[Project] (Title, Description, Topic)
                           values (@Title, @Description, @Topic);";

            return SQLDataAccess.SaveData(sql, data);
        }

        public static List<ProjectModel> LoadProjects()
        {
            string sql = @"select Title, Description, Topic
                            from [dbo].[Project];";

            return SQLDataAccess.LoadData<ProjectModel>(sql);
        }
    }
}
