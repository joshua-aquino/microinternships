﻿using DataLibrary.DataAccess;
using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLibrary.BusinessLogic
{
    public static class UserProcessor 
    {
        public static int CreateUser(int nNum, string firstName, 
            string lastName, string email)
        {
            UserModel data = new UserModel
            {
                NNum = nNum,
                FirstName = firstName,
                LastName = lastName,
                Email = email
            };

            string sql = @"insert into [dbo].[User] (NNum, FirstName, LastName, Email)
                           values (@NNum, @FirstName, @LastName, @Email);";

            return SQLDataAccess.SaveData(sql, data);
        }

        public static List<UserModel> LoadUsers()
        {
            string sql = @"select NNum, FirstName, LastName, Email
                            from [dbo].[User];";

            return SQLDataAccess.LoadData<UserModel>(sql);
        }
    }
}
